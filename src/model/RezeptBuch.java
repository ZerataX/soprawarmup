package model;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import model.Rezept;

/**
 * RezeptBuch ist eine Klasse die dafür verantwortlich ist die Rezepte zu laden und zu speichern.
 * Sie verwendet die externe Bibliothek simple-json <a href="https://code.google.com/archive/p/json-simple/">https://code.google.com/archive/p/json-simple/</a>
 * @author Jona Abdinghoff
 *
 */
public class RezeptBuch {
	private List<Rezept> rezepte = new ArrayList<Rezept>();
	private String path = "rezeptbuch.json";
	
	public List<Rezept> getRezepte () {
		return rezepte;
	}
	
	public void setRezepte (List<Rezept> rezepte) {
		this.rezepte = rezepte;
	}
	/**
	 * Speichert die Rezepte in einer json relativ zum Ausführungsort in <code>rezeptbuch.json</code>
	 * @throws IOException falls die Datei nicht gespeichert werden kann (z.B. fehlende Schreibberechtigungen)
	 */
	public void save() throws IOException{
		JSONObject obj = new JSONObject();
		JSONArray rezepteJSON = new JSONArray();
		for (int i = 0; i < rezepte.size(); i++) {
			Rezept rezept = rezepte.get(i);
			rezepteJSON.add(rezept.getJSONObject());
		}
		
		obj.put("rezepte", rezepteJSON);
		obj.put("version", 1);
		try (FileWriter file = new FileWriter(path)) {
			file.write(obj.toJSONString());
			System.out.println("Successfully Copied JSON Object to File...");
			System.out.println("\nJSON Object: " + obj);
		}
	}
	
	/**
	 * Lädt die Rezepte relativ zum Ausführungsort aus <code>rezeptbuch.json</code>.
	 * @throws IOException Falls die Datei nicht gelesen werden kann
	 * @throws ParseException Falls die JSON nicht valide ist
	 */
	public void load() throws IOException, ParseException {
		File f = new File(path);
		if(f.exists() && !f.isDirectory()) { 
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(new FileReader(path));

	        JSONObject jsonObject = (JSONObject) obj;
	        JSONArray rezepteJSON = (JSONArray) jsonObject.get("rezepte");
	        rezepte.clear();
	        for (int i = 0; i < rezepteJSON.size(); i++) {
	        	JSONObject rezept = (JSONObject) rezepteJSON.get(i);
	        	
	        	String name = (String) rezept.get("name");
	        	List<String> Zutaten = (List<String>) rezept.get("zutaten");
	        	List<String> Schritte = (List<String>) rezept.get("schritte");
	        	
				rezepte.add(new Rezept(name, Zutaten, Schritte));
			}
		}	
	}
}
