package model;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;

/**
 * Rezept Klasse, die Name Zutaten und Schritte eines Rezeptes speichert und sich selbst als JSONOBject ausgeben kann
 * @author Jona Abdinghoff
 *
 */
public class Rezept {
	String name = "";
	private List<String> zutaten = new ArrayList<String>();
	private List<String> schritte = new ArrayList<String>();

	public Rezept(String name) {
		this.name = name;
	}

	public Rezept(String name, List<String> zutaten, List<String> schritte) {
		this.name = name;
		this.zutaten = zutaten;
		this.schritte = schritte;
	}

	public void setZutaten(ArrayList<String> zutaten) {
		this.zutaten = zutaten;
	}

	public List<String> getZutaten() {
		return zutaten;
	}

	public void setSchritte(ArrayList<String> schritte) {
		this.schritte = schritte;
	}

	public List<String> getSchritte() {
		return schritte;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gibt die Instanz als valides JSONObject zurück.
	 * @return das JSONObject, dass die Instanz repräsentiert
	 */
	public JSONObject getJSONObject() {
		JSONObject obj = new JSONObject();

		obj.put("name", name);
		obj.put("schritte", schritte);
		obj.put("zutaten", zutaten);
		
	    return obj;
	}
}
