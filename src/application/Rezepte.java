package application;

import model.Rezept;
import model.RezeptBuch;

import java.io.IOException;
import java.util.ArrayList;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.*;
import javafx.event.*;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.TextFieldListCell;
import javafx.scene.layout.*;
import javafx.stage.Stage;
import javafx.util.StringConverter;

/**
 * Hauptklasse die eine GUI implementiert zum verwalten eines Rezeptbuches.
 * 
 * @author Jona Abdinghoff
 *
 */
public class Rezepte extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		stage.setTitle("Rezepte");

		final VBox root = new VBox();

		// Buttons
		Button buttonNew = new Button("Neu");
		Button buttonSave = new Button("Speichern");
		Button buttonAddZutat = new Button("Neue Zutat");
		Button buttonAddSchritt = new Button("Neuer Schritt");

		FlowPane flow = new FlowPane();
		flow.setVgap(8);
		flow.setHgap(4);
		flow.getChildren().addAll(buttonNew, buttonSave);

		// Listen
		RezeptBuch RezeptBuch = new RezeptBuch();
		RezeptBuch.load();
		ObservableList<Rezept> RezeptListe = FXCollections.observableArrayList(RezeptBuch.getRezepte());

		ListView<String> rezeptZutaten = new ListView<String>();
		ListView<String> rezeptSchritte = new ListView<String>();
		ListView<Rezept> rezepte = new ListView<Rezept>(RezeptListe);

		/*
		 * die Rezepte ListView als bearbeitbar markieren,
		 * d.h. bei doppel Klicken lässt sich das Item umbennen
		 * bei leerem Namen soll das Item gelöscht werden.
		 */
		rezepte.setEditable(true);
		/*
		 * Um die ListView<Rezept> als Strings in der ListView anzuzeigen müssen wir für
		 * jedes Rezept-Item den Namen des Items abfragen
		 */
		rezepte.setCellFactory(listView -> {
			TextFieldListCell<Rezept> cell = new TextFieldListCell<>();
			cell.setConverter(new StringConverter<Rezept>() {
				@Override
				/*
				 * Abfragen der String repräsentation für das Item
				 */
				public String toString(Rezept item) {
					return item.getName();
				}

				@Override
				/*
				 * Nach Bearbeiten Namen des Items neu setzen
				 */
				public Rezept fromString(String string) {
					Rezept item = cell.getItem();
					item.setName(string);
					return item;
				}
			});
			return cell;
		});

		/*
		 * Bei auswählen des Rezept-Items die entsprechenden Zutaten und Schritte
		 * anzeigen.
		 */
		rezepte.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Rezept>() {
			public void changed(ObservableValue<? extends Rezept> observable, Rezept oldValue, Rezept newValue) {
				if (newValue == null) {
					// Falls alle Rezept-Items gelöscht erzeuge neues Gericht
					newValue = new Rezept("Neues Gericht");
					rezepte.getItems().add(newValue);
				}
				ObservableList<String> zutaten = FXCollections.observableArrayList(newValue.getZutaten());
				ObservableList<String> schritte = FXCollections.observableArrayList(newValue.getSchritte());

				rezeptZutaten.setItems(zutaten);
				rezeptSchritte.setItems(schritte);
			}
		});

		// Bei Enter drücken nach Bearbeiten von Rezept-Item (akzeptieren der
		// Bearbeitung)
		rezepte.setOnEditCommit(new EventHandler<ListView.EditEvent<Rezept>>() {
			@Override
			public void handle(ListView.EditEvent<Rezept> t) {
				String newValue = t.getNewValue().getName();
				if (newValue != null && !newValue.isEmpty()) {
					// Falls der bearbeitete Name des Rezeptes nicht leer ist, update mit neuen
					// Namen.
					rezepte.getItems().set(t.getIndex(), t.getNewValue());
					System.out.println("setOnEditCommit");
				} else {
					// Falls er hingegen leer ist, entferne das Rezept-Item
					rezepte.getItems().remove(t.getIndex());
					System.out.println("setOnEditDelete");
				}
			}

		});

		// Bei wegklicken nach Bearbeiten von Rezept-Item (ablehnen der Bearbeitung)
		rezepte.setOnEditCancel(new EventHandler<ListView.EditEvent<Rezept>>() {
			@Override
			public void handle(ListView.EditEvent<Rezept> t) {
				System.out.println("setOnEditCancel");
			}
		});

		// die RezeptZutaten ListView als bearbeitbar markieren
		rezeptZutaten.setEditable(true);
		rezeptZutaten.setCellFactory(TextFieldListCell.forListView());

		// akzeptieren der Bearbeitung
		rezeptZutaten.setOnEditCommit(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> t) {
				String newValue = t.getNewValue();
				Rezept rezept = rezepte.getSelectionModel().getSelectedItem();

				if (newValue != null && !newValue.isEmpty()) {
					// bearbeiteter Zutat ist nicht leer, also setze als neue Zutat
					rezeptZutaten.getItems().set(t.getIndex(), t.getNewValue());
					ArrayList<String> Zutaten = new ArrayList<String>(rezeptZutaten.getItems());

					rezept.setZutaten(Zutaten);
					System.out.println("setOnEditCommit");
				} else {
					// bearbeiteter Zutat ist leer, entferne Zutat-Item
					rezeptZutaten.getItems().remove(t.getIndex());
					System.out.println("setOnEditDelete");
				}
			}

		});

		// ablehnen der Bearbeitung
		rezeptZutaten.setOnEditCancel(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> t) {
				System.out.println("setOnEditCancel");
			}
		});

		// die RezeptSchritte ListView als bearbeitbar markieren
		rezeptSchritte.setEditable(true);
		rezeptSchritte.setCellFactory(TextFieldListCell.forListView());

		// akzeptieren der Bearbeitung
		rezeptSchritte.setOnEditCommit(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> t) {
				String newValue = t.getNewValue();
				Rezept rezept = rezepte.getSelectionModel().getSelectedItem();
				if (newValue != null && !newValue.isEmpty()) {
					// bearbeiteter Schritt ist nicht leer, also setze als neuen Schritt
					rezeptSchritte.getItems().set(t.getIndex(), t.getNewValue());
					ArrayList<String> Schritte = new ArrayList<String>(rezeptSchritte.getItems());

					rezept.setSchritte(Schritte);
					System.out.println("setOnEditCommit");
				} else {
					// bearbeiteter Schritt ist leer, entferne Schritt-Item
					rezeptSchritte.getItems().remove(t.getIndex());
					System.out.println("setOnEditDelete");
				}
			}
		});

		// ablehnen der Bearbeitung
		rezeptSchritte.setOnEditCancel(new EventHandler<ListView.EditEvent<String>>() {
			@Override
			public void handle(ListView.EditEvent<String> t) {
				System.out.println("setOnEditCancel");
			}
		});

		buttonNew.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				rezepte.getItems().add(new Rezept("Neues Gericht"));
			}
		});

		buttonSave.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				RezeptBuch.setRezepte(new ArrayList<Rezept>(RezeptListe));
				try {
					RezeptBuch.save();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		});

		buttonAddZutat.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				// kontrollieren ob ein Rezept-Item überhaupt selektiert ist, ansonsten kein
				// neues Item erstellen
				if (rezepte.getSelectionModel().getSelectedItem() != null) {
					ArrayList<String> zutaten = new ArrayList<String>(rezeptZutaten.getItems());
					Rezept rezept = rezepte.getSelectionModel().getSelectedItem();

					zutaten.add("Neue Zutat");
					rezeptZutaten.getItems().add("Neue Zutat");
					rezept.setZutaten(zutaten);
				}
			}
		});

		buttonAddSchritt.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				// kontrollieren ob ein Rezept-Item überhaupt selektiert ist, ansonsten kein
				// neues Item erstellen
				if (rezepte.getSelectionModel().getSelectedItem() != null) {
					ArrayList<String> schritte = new ArrayList<String>(rezeptSchritte.getItems());
					Rezept rezept = rezepte.getSelectionModel().getSelectedItem();

					schritte.add("Neuer Schritt");
					rezeptSchritte.getItems().add("Neuer Schritt");
					rezept.setSchritte(schritte);
				}
			}
		});

		// Alle Items auf die Szene platzieren
		root.getChildren().addAll(rezepte, flow, rezeptZutaten, buttonAddZutat, rezeptSchritte, buttonAddSchritt);

		final Scene scene = new Scene(root, 500, 500);

		// Szene darstellen
		stage.setScene(scene);
		stage.show();
	}
}